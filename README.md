ng2-card
========

Use [Bootstrap cards](https://v4-alpha.getbootstrap.com/components/card/) with Angular 2 and less boilerplate HTML.

Prerequisites
-------------

Install the 'ng2-card' module:

`npm install --save ng2-bootstrap-card`

Declare the card component in a module where you want to use it:

```
import { CardComponent } from 'ng2-bootstrap-card/ng2-bootstrap-card';
...
@NgModule({
  ...
  declarations: [
    ...
    CardComponent
  ]
  ...
})
```

To use with a module loader like SystemJS, add the module to your config file, eg:

```
System.config({
  ...
  map: {
    ...
    'ng2-bootstrap-card': 'node_modules/ng2-bootstrap-card/'
  },
  packages: {
    ...
    'ng2-bootstrap-card': {
      defaultExtension: 'js'
    }
  }
});
```

Include [Bootstrap v4 CSS](https://v4-alpha.getbootstrap.com/) in your index.html file.

Usage
-----

A minimal example:

```
<card>
  This is a Bootstrap card
</card>
```
  
TODOs
-----

* Add more card features
* Add directives for more customizations
* Consider setting `margin: 0px` on last-child of '.card-text' elements


