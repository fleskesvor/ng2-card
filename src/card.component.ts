import { Component, Input } from '@angular/core';

@Component({
  selector: 'card',
  template: `
    <div class="{{className}}" [style.paddingLeft]="paddingLeft" [style.paddingRight]="paddingRight">
      <div class="card">

        <ng-content select="[header]"></ng-content>
        <div *ngIf="header" class="card-header">{{header}}</div>

        <div class="card-block" [ngStyle]="getCardBlockStyles()">
          <h4 *ngIf="title" class="card-title">
            {{title}}
          </h4>
          <ng-content></ng-content>
        </div>

        <div class="card-footer">{{footer}}</div>

      </div>
    </div>
  `
})

export class CardComponent {
  @Input() className: string = '';
  @Input() header: string = '';
  @Input() footer: string = '';
  @Input() title: string;
  @Input() paddingLeft = '0px';
  @Input() paddingRight = '0px';
  @Input() color = '#000000';
  @Input() backgroundColor = '#ffffff';

  private getCardBlockStyles() {
    return {
      'color': this.color,
      'background-color': this.backgroundColor
    };
  }
}
